
//script for slider

var slideIndex = 1;
		showDivs(slideIndex);

		function plusDivs(n) {
		  showDivs(slideIndex += n);
		}

		function showDivs(n) {
		  var i;
		  var x = document.getElementsByClassName("mySlides");
		  if (n > x.length) {slideIndex = 1}    
		  if (n < 1) {slideIndex = x.length}
		  for (i = 0; i < x.length; i++) {
		     x[i].style.display = "none";  
		  }
		  x[slideIndex-1].style.display = "block";  
		}

//function to validate the Name

function validateName(){
	var name=document.getElementById("uname").value;

	//check if the name is empty

	if (name.length==0){
		show("namePrompt");
		producePrompt("Name is Required","namePrompt","red");
		setTimeout(function()
		{
			hide("namePrompt");
			
		},2000);
		return false;
	}

	//check if the name has first name and last name
	//'^' is the start of the the regex
	//'$' is the end of regex
	//'s' is for space

	if (!name.match(/^[A-Za-z]*$/)){
		show("namePrompt");
		producePrompt("your name please", "namePrompt","red");
		setTimeout(function()
		{
			hide("namePrompt");
			
		},2000);
		return false;
	}

	//if everythin is correct how the label as welcome
	show("namePrompt");
	producePrompt("Welcome " + name, "namePrompt","green");
	setTimeout(function()
		{
			hide("namePrompt");
			
		},2000);
		return true;

}



//function to validate Email Id
function validateEmail(){

	var email=document.getElementById("uemail").value;

	//check if input is empty

	if(email.length==0){
		show("emailPrompt");

		producePrompt("Email is required","emailPrompt","red");
		setTimeout(function()
		{
			hide("emailPrompt");
			
		},2000);
		return false;
	}

	//check for proper email address

	if(!email.match(/^[a-z\._\-0-9]*[@][A-Za-z]*[\.][a-z]{2,3}$/)){
		producePrompt("Invalid Email Address","emailPrompt","red");
		
		return false;
	}

	//valid email
	show("emailPrompt");
	producePrompt("Valid Email-ID","emailPrompt","green");
	setTimeout(function()
		{
			hide("emailPrompt");
			
		},2000);
	return true;
	
	
}


function CheckPassword(inputtxt)   
{   
	var inputtxt=document.getElementById("psw");
	var passw=  /^[A-Za-z]\w{7,14}$/;  
	if(inputtxt.value.match(passw))   
	{   
	//alert('Correct, try another...')  
	show("passPrompt");
	producePrompt("Valid Password","passPrompt","green");
	setTimeout(function()
		{
			hide("passPrompt");
			
		},2000);
	return true;  
	}  
	else  
	{   
	producePrompt("Please choose good password","passPrompt","red");  
	return false;  
}  
}

//***************PASSWORD CHECK FOR SIGNIN PAGE********************

function CheckPassword1(inputtxt)   
{   
	var inputtxt=document.getElementById("psw1");
	var passw1=  /^[A-Za-z]\w{7,14}$/;  
	if(inputtxt.value.match(passw1))   
	{   
	//alert('Correct, try another...') 
	 show("passPrompt1");
	producePrompt("Valid Password","passPrompt1","green");
	setTimeout(function()
		{
			hide("passPrompt1");
			
		},2000);
	return true;  
	}  
	else  
	{   
	producePrompt("Please choose good password","passPrompt1","red");  
	return false;  
}  
}

function CheckPassword2(inputtxt)   
{   
	var inputtxt1=document.getElementById("psw1").value;
	var inputtxt2=document.getElementById("psw2").value;
	if(inputtxt1===inputtxt2)   
	{   
	 show("passPrompt2"); 
	producePrompt("Passwords Match","passPrompt2","green");
	setTimeout(function()
		{
			hide("passPrompt2");
			
		},2000);
	return true;  
	}  
	else  
	{   
	producePrompt("Password mismatch","passPrompt2","red");  
	return false;  
}  
}

//Function for the submit button validation

function validateSignup(){

	if(!validateName() || !validateEmail())
	{

		show("signupProm");
		producePrompt("Please fill all the details","signupProm","red");
		setTimeout(function()
		{
			hide("signupProm");
			
		},2000);
	}

	else{
  	window.location.replace("user.html");
  }

}

function show(id){
	document.getElementById(id).style.display="block";
}

function hide(id){
	document.getElementById(id).style.display="none";
}




//Function for the submit button validation of signin page

function validateSignin(){
	var x=document.getElementById("sel");
	var i=x.selectedIndex;
	var person=x.options[i].text;
		//alert(person);

	if(!validateEmail())
	{
		show("signupProm");
		producePrompt("Please fill all the details","signupProm","red");
		setTimeout(function()
		{
			hide("signupProm");
			
		},2000);
	}

	else if(person="user")
	 {
  	//producePrompt("Success Your Comment has been submitted","success","green");

  	window.location.replace("user.html");
  	}

  	else if(person="admin")
	 {
  	window.location.replace("admin.html");
  	}

  	else if(person="partner")
	 {
  	window.location.replace("partner.html");
  	}



}

function show(id){
	document.getElementById(id).style.display="block";
}

function hide(id){
	document.getElementById(id).style.display="none";
}




//function to produce prompt about the user input
function producePrompt(message,promptLocation,color){
	document.getElementById(promptLocation).innerHTML=message;
	document.getElementById(promptLocation).style.color=color;
}
